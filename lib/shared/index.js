'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.urlHelper = exports.isDefined = undefined;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Проверка на установленное значение.
 *
 * @param value Значение для проверки
 * @returns {boolean} Результат.
 */
var isDefined = exports.isDefined = function isDefined(value) {
  return !_lodash2.default.isUndefined(value) && !_lodash2.default.isNull(value);
};

/**
 * URl-хелпер для преобразования в маршрут для Yii2.
 *
 * @param route Маршрут для обработки.
 * @returns {string} Обработанный маршрут.
 */
/**
 * Импортируем Lodash.
 */
var urlHelper = exports.urlHelper = function urlHelper(route) {
  return location.pathname.replace(/[\w-]+\/[\w-]+\\*$/, route);
};