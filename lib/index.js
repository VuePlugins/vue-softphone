'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/**
 * Плагин для Vue.js для управления утилитой SoftPhone, начиная с версии 6.3.
 *
 */
var axios = require('axios');
var helpers = require("./shared");

var VueSoftphone = {
    /**
     * Иньекция плагина.
     *
     * @param Vue Vue.js версии 5.2.+
     * @param options Начальные параметры.
     */
    install: function install(Vue, options) {
        var _this = this;

        Vue.mixin({
            mounted: function mounted() {
                // console.info('SoftPhone Vue plugin is loading...');
                this.$softphone.init();
                // console.info('SoftPhone Vue plugin was loaded!');
            }
        });
        /**
         * Определяем плагин как $softphone.
         *
         * @type {{data: {actionRoutes: {transferCreate: {url: string}, toneDial: {url: string}, closeCall: {url: string}}}, transferCloseHelper: function(*, *=, *=), setStatus: function(*=, *=, *=), originalTransferFunction: function(), originalTransferWithCallBackFunction: function(), originalConsultationTransferFunction: function(), originalToneDialFunction: function(), getCallId: function(): *, canClose: function(*=), closeOnCallEnd: function(*=), blockClosing: function(): (blockClosing|(function())), submitTimeout: function(*=), workType: function(*=), defaultWorkType: function(): (defaultWorkType|(function())), factualCompletion: function(): (factualCompletion|(function())), formClosing: function(): (formClosing|(function())), limitValueArrived: function(): (limitValueArrived|(function())), submitForm: function(*=), wrapupTime: function(): (wrapupTime|(function())), idleHold: function(): (idleHold|(function())), init(): boolean, sendToCallBack(*=, *=): void, checkClientCallId(): boolean, initSoftPhone(): boolean, isInSoftPhone(*=): boolean, setClientCallId(*=): boolean}}
         */
        Vue.prototype.$softphone = {
            data: _extends({
                client: {
                    clientCallId: window.ID_CALL
                },
                actionRoutes: {
                    transferCreate: {
                        url: 'call/transfer-create'
                    },
                    toneDial: {
                        url: 'call/tone-dial'
                    },
                    closeCall: {
                        url: 'call/close'
                    }
                }
            }, options),

            /**
             * Закрыть трансфер.
             *
             * @param transferResult Результат трансфера.
             * @param clientCallId Call id клиента.
             *
             * @param callback callback-функция.
             */
            transferCloseHelper: function transferCloseHelper(transferResult) {
                var clientCallId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
                var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;
                return axios.get('call/transfer-close', {
                    params: {
                        transferResult: transferResult,
                        ID_CALL: helpers.isDefined(clientCallId) ? clientCallId : window.clientCallId
                    }
                }).then(function (response) {
                    return _this.sendToCallBack(response.data, callback);
                }).catch(function (error) {
                    return console.warn('Ошибки при отправке запроса на "Закрытие трансфера"!', error);
                });
            },

            /**
             * Установит статус клиента.
             *
             * @param status Статус.
             * @param clientCallId Call Id клиента.
             *
             * @param callback callback-функция.
             */
            setStatus: function setStatus(status) {
                var clientCallId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
                var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;
                return axios.get('call/set-status', {
                    params: {
                        ID_STATUS: status,
                        ID_CALL: helpers.isDefined(clientCallId) ? clientCallId : window.clientCallId
                    }
                }).then(function (response) {
                    return _this.sendToCallBack(response.data, callback);
                }).catch(function (error) {
                    return console.warn('Ошибки при отправке запроса на "Установку статуса"!', error);
                });
            },

            /**
             * Оригинальная функция трансфера.
             */
            originalTransferFunction: function originalTransferFunction() {},

            /**
             * Оригинальная функция трансфера с callback'ом.
             */
            originalTransferWithCallBackFunction: function originalTransferWithCallBackFunction() {},

            /**
             * Оригинальная функция транфсера-консультации.
             */
            originalConsultationTransferFunction: function originalConsultationTransferFunction() {},

            /**
             * Оригинальная функция тонового набора.
             */
            originalToneDialFunction: function originalToneDialFunction() {},

            /**
             * Идентификатор вызова (то же что и атрибут eggId
             * объекта EggPrototype).
             * @returns {*}
             */
            getCallId: function getCallId() {
                return phoneControllerService.callId;
            },

            /**
             * Указывает на то, разрешено ли закрывать эту
             * страницу
             * @param value
             * @returns {undefined|*}
             */
            canClose: function canClose() {
                var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

                if (value === undefined) {
                    return phoneControllerService.canClose;
                } else {
                    phoneControllerService.canClose = value;
                    return phoneControllerService.canClose;
                }
            },
            /**
             * SoftPhone завершает вызов с идентификатором, заданным
             * параметром call_id.
             *
             * @returns {boolean}
             */
            hangup: function hangup() {
                phoneControllerService.hangup();
                return true;
            },

            /**
             * Завершение вызова, находящегося в
             * поствызывной обработке.
             */
            closeCall: function closeCall() {
                phoneControllerService.close();
            },

            /**
             * Указывает на то, нужно ли удалять вызов по его
             * завершению.
             *
             * Примечание Если вызов уже завершён, то при
             * присвоении данному параметру значения true ,
             * закрытие страницы не произойдёт, так же если
             * параметр canClose имеет значение false , то
             * закрытие страницы также не произойдёт
             *
             * @param value
             * @returns {undefined|*}
             */
            closeOnCallEnd: function closeOnCallEnd() {
                var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

                if (value === undefined) {
                    return phoneControllerService.closeOnCallEnd;
                } else {
                    phoneControllerService.closeOnCallEnd = value;
                    return phoneControllerService.closeOnCallEnd;
                }
            },

            /**
             * Указывает на то, разрешено ли блокировать закрытие
             * страницы (устанавливается через параметры
             * ActiveXParams).
             *
             * @returns {blockClosing|(function())}
             */
            blockClosing: function blockClosing() {
                return phoneControllerService.blockClosing;
            },

            /**
             * Время для поствызывной обработки в режиме работы
             * limitValueArrived.
             *
             * @param value
             * @returns {undefined|*}
             */
            submitTimeout: function submitTimeout() {
                var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

                if (value === undefined) {
                    return phoneControllerService.submitTimeout;
                } else {
                    phoneControllerService.submitTimeout = value;
                    return phoneControllerService.submitTimeout;
                }
            },

            /**
             * Режим работы.
             *
             * @param value
             * @returns {undefined|*}
             */
            workType: function workType() {
                var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

                if (value === undefined) {
                    return phoneControllerService.workType;
                } else {
                    phoneControllerService.workType = value;
                    return phoneControllerService.workType;
                }
            },

            /**
             * Режим работы по умолчанию.
             *
             * @returns {defaultWorkType|(function())}
             */
            defaultWorkType: function defaultWorkType() {
                return phoneControllerService.defaultWorkType;
            },

            /**
             * Идентификатор режима работы factualCompletion.
             *
             * @returns {factualCompletion|(function())}
             */
            factualCompletion: function factualCompletion() {
                return phoneControllerService.factualCompletion;
            },

            /**
             * Идентификатор режима работы formClosing.
             *
             * @returns {formClosing|(function())}
             */
            formClosing: function formClosing() {
                return phoneControllerService.formClosing;
            },

            /**
             * Идентификатор режима работы limitValueArrived.
             *
             * @returns {limitValueArrived|(function())}
             */
            limitValueArrived: function limitValueArrived() {
                return phoneControllerService.limitValueArrived;
            },

            /**
             * Указывает на то, нужно ли при завершении вызова
             * эмулировать нажатие на кнопку с id 'save', если она
             * присутствует на форме.
             *
             * @param value
             * @returns {undefined|*}
             */
            submitForm: function submitForm() {
                var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

                if (value === undefined) {
                    return phoneControllerService.submitForm;
                } else {
                    phoneControllerService.submitForm = value;
                    return phoneControllerService.submitForm;
                }
            },

            /**
             * Время поствызывной обработки.
             *
             * @returns {wrapupTime|(function())}
             */
            wrapupTime: function wrapupTime() {
                return phoneControllerService.wrapupTime;
            },

            /**
             * Указывает на то, что удержание вызова не
             * производится.
             *
             * @returns {idleHold|(function())}
             */
            idleHold: function idleHold() {
                return phoneControllerService.idleHold;
            },

            /**
             * Осуществление исходящего вызова на номер number.
             *
             * @param number
             */
            call: function call(number) {
                phoneControllerService.call(number);
            },


            /**
             * Изменение состояния SoftPhone на newState с причиной reason.
             * Параметр newState может содержать следующие значения:
             *
             * 'normal'
             * 'away'
             * 'dnd'
             * 'custom1'
             * 'custom2'
             * 'custom3'
             *
             * @param newState
             * @param reason
             */
            changeState: function changeState(newState, reason) {
                phoneControllerService.changeState(newState, reason);
            },


            /**
             * Выход из SoftPhone.
             */
            logout: function logout() {
                phoneControllerService.logout();
            },


            /**
             * Закрытие приложения SoftPhone.
             */
            exit: function exit() {
                phoneControllerService.exit();
            },


            /**
             * Переоткрытие звукового устройства.
             */
            reopenAudioDevice: function reopenAudioDevice() {
                phoneControllerService.reopenAudioDevice();
            },


            /**
             * Отправка сообщения (alert) шине, где level может принимать
             * следующие значения:
             *
             * CRIT — 0;
             * ERROR — 1;
             * WARN — 2;
             * INFO — 3;
             * DEBUG — 4.
             *
             * @param level
             * @param message
             * @param service
             */
            sendAlert: function sendAlert(level, message, service) {
                phoneControllerService.sendAlert(level, message, service);
            },


            /**
             * Отправка факса на номер number , где fileOrUrl — путь до
             * локально расположенного файла, или URL (осуществление
             * вызова на указанный номер и отправка факса).
             *
             * @param number
             * @param fileOrUr
             */
            sendFaxOnNumber: function sendFaxOnNumber(number, fileOrUr) {
                phoneControllerService.sendFaxOnNumber(number, fileOrUr);
            },


            /**
             * Отправка текстового сообщения в вызов (по протоколу SIP/H323).
             *
             * @param message
             */
            sendVoipMessage: function sendVoipMessage(message) {
                phoneControllerService.sendVoipMessage(message);
            },


            /**
             * Отправка записи на вкладку Отчёт в SoftPhone
             *
             * @param level
             * @param message
             * @param params
             */
            oftphoneLog: function oftphoneLog(level, message) {
                var _phoneControllerServi;

                for (var _len = arguments.length, params = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
                    params[_key - 2] = arguments[_key];
                }

                (_phoneControllerServi = phoneControllerService).oftphoneLog.apply(_phoneControllerServi, [level, message].concat(_toConsumableArray(params)));
            },


            /**
             * Возвращает параметр вызова с именем key.
             *
             * @param key
             */
            getPageParam: function getPageParam(key) {
                phoneControllerService.getPageParam(key);
            },


            /**
             * Задает параметру с именем key значением value.
             *
             * @param key
             * @param value
             */
            setPageParam: function setPageParam(key, value) {
                phoneControllerService.setPageParam(key, value);
            },


            /**
             * Включение/выключение режима безопасности (отключение записи
             * разговора). Параметр enable может принимать следующие
             * значения:
             *
             * true — включение режима безопасности.
             * false — выключение режима безопасности (по умолчанию).
             *
             * Примечание Метод, без каких-либо последствий, может быть
             * вызван несколько раз подряд.
             *
             * @param enable
             */
            setSessionSecureMode: function setSessionSecureMode(enable) {
                phoneControllerService.setSessionSecureMode(enable);
            },


            /**
             * Инициализация и применение основных настроек сервиса.
             */
            init: function init() {
                var _this2 = this;

                // Проверяем/инициализируем экземпляр SoftPhone.
                var modulesChecking = this.isInSoftPhone();
                // если что-то пошло не так...
                if (!modulesChecking) {
                    alert('Проблемой с загрузкой модулей!');
                    return false;
                }

                /**
                 * Создание функций хелперов.
                 */
                if (helpers.isDefined(phoneControllerService.onStatusChange)) {
                    phoneControllerService.onStatusChange = function (status) {
                        return _this2.setStatus(status);
                    };
                }

                if (helpers.isDefined(phoneControllerService.onStatusChanged)) {
                    phoneControllerService.onStatusChanged = function (status) {
                        return _this2.setStatus(status);
                    };
                }

                phoneControllerService.onDropCall = function () {
                    return axios.get(_this2.data.actionRoutes.closeCall.url, {
                        params: {
                            ID_CALL: _this2.data.client.clientCallId
                        }
                    }).then(function (response) {
                        return _this2.sendToCallBack(response.data, callback);
                    }).catch(function (error) {
                        return console.warn('Ошибки при отправке запроса на "Закрытие звонка"!', error);
                    });
                };

                /**
                 * Функция трансфера.
                 *
                 * @param phone Номер телефона.
                 */
                phoneControllerService.transfer = function (phone) {

                    phoneControllerService.currentTransferType = 'Обычное перенаправление';
                    phoneControllerService.currentTransferPhone = phone;

                    _this2.originalTransferFunction(phone);
                };
                /**
                 * Трансфер с callback.
                 *
                 * @param phone Номер телефона.
                 */
                phoneControllerService.transferWithCallBack = function (phone) {

                    phoneControllerService.currentTransferType = 'Перенаправление с возвратом';
                    phoneControllerService.currentTransferPhone = phone;

                    _this2.originalTransferWithCallBackFunction(phone);
                };

                /**
                 * Трансфер с консультацией.
                 *
                 * @param phone Номер телефона.
                 */
                phoneControllerService.consultationTransfer = function (phone) {

                    phoneControllerService.currentTransferType = 'Перенаправление с консультацией';
                    phoneControllerService.currentTransferPhone = phone;

                    _this2.originalConsultationTransferFunction(phone);
                };

                this.originalTransferFunction = phoneControllerService.transfer;
                this.originalTransferWithCallBackFunction = phoneControllerService.transferWithCallBack;
                this.originalConsultationTransferFunction = phoneControllerService.consultationTransfer;
                this.originalToneDialFunction = phoneControllerService.toneDial;

                /**
                 * Обработка трансфера.
                 */
                phoneControllerService.onTransferCall = function () {
                    axios.get(_this2.data.actionRoutes.transferCreate.url, {
                        params: {
                            transferType: phoneControllerService.currentTransferType,
                            transferPhone: phoneControllerService.currentTransferPhone,
                            ID_CALL: _this2.data.client.clientCallId
                        }
                    }).then(function (response) {

                        phoneControllerService.currentTransferType = null;
                        phoneControllerService.currentTransferPhone = null;

                        _this2.sendToCallBack(response.data, callback);
                    }).catch(function (error) {
                        return console.warn('Ошибки при отправке запроса на "Закрытие звонка"!', error);
                    });
                };

                // Успешный трансфер.
                phoneControllerService.onTransferSucceed = function () {
                    return _this2.transferCloseHelper('Удачно');
                };
                // С ошибкой.
                phoneControllerService.onTransferFailed = function () {
                    return _this2.transferCloseHelper('Не удачно');
                };
                // Был возвращён.
                phoneControllerService.onTransferCallReturned = function () {
                    return _this2.transferCloseHelper('Возвращен');
                };

                /**
                 * Тоновый набор.
                 *
                 * @param phone Номер телефона.
                 */
                phoneControllerService.toneDial = function (phone) {
                    axios.get(_this2.data.actionRoutes.toneDial.url, {
                        PHONE: phone,
                        ID_CALL: _this2.data.client.clientCallId
                    });
                    _this2.originalToneDialFunction(phone);
                };
            },


            /**
             * Передать параметр в callback.
             *
             * @param data Данные для прокидки.
             * @param callback callback-функция.
             */
            sendToCallBack: function sendToCallBack(data, callback) {
                if (helpers.isDefined(callback)) {
                    callback(data);
                }
            },


            /**
             * Проверка на наличие установленного callId.
             *
             * @returns {boolean}
             */
            checkClientCallId: function checkClientCallId() {
                return helpers.isDefined(this.data.client.clientCallId);
            },


            /**
             * Инициализация SoftPhone.
             *
             * @returns {boolean} Результат инициализации.
             */
            initSoftPhone: function initSoftPhone() {
                try {
                    window.phoneControllerService = {
                        onDropCall: function onDropCall() {},
                        close: function close() {
                            this.onDropCall();
                            window.close();
                        },
                        transfer: function transfer() {},
                        transferWithCallBack: function transferWithCallBack() {},
                        consultationTransfer: function consultationTransfer() {},
                        toneDial: function toneDial() {},
                        setMusicOnHold: function setMusicOnHold() {}
                    };
                } catch (exception) {
                    console.warn('Не удалось создать экземпляр SoftPhone!', exception);
                    return false;
                }

                console.warn('\u0423\u0442\u0438\u043B\u0438\u0442\u0430 SofPhone \u043D\u0435 \u0431\u044B\u043B\u0430 \u043E\u0431\u043D\u0430\u0440\u0443\u0436\u0435\u043D\u0430! \u0421\u043E\u0437\u0434\u0430\u0451\u043C \u0441\u0432\u043E\u0439 \u044D\u043A\u0437\u0435\u043C\u043F\u043B\u044F\u0440, \u043C\u0430\u043B\u043E \u043B\u0438...');
                return true;
            },


            /**
             * Проверка на наличие основного объекта SoftPhone: phoneControllerService.
             *
             * @param createIfNotExists Если объект не найден, создать его или нет.
             * @returns {boolean} Результат.
             */
            isInSoftPhone: function isInSoftPhone() {
                var createIfNotExists = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

                var softPhoneExists = typeof phoneControllerService !== 'undefined';
                if (!softPhoneExists && createIfNotExists) {
                    softPhoneExists = this.initSoftPhone();
                }
                return softPhoneExists;
            },


            /**
             * Установка CallId клиента.
             *
             * @param newClientCallId CallId клиента.
             * @returns {boolean} Результат.
             */
            setClientCallId: function setClientCallId(newClientCallId) {

                if (!helpers.isDefined(newClientCallId)) return false;

                this.data.client.clientCallId = newClientCallId;

                return true;
            }
        };
    }
};

exports.default = VueSoftphone;