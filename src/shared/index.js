/**
 * Импортируем Lodash.
 */
import _ from 'lodash';

/**
 * Проверка на установленное значение.
 *
 * @param value Значение для проверки
 * @returns {boolean} Результат.
 */
export const isDefined = (value) => !_.isUndefined(value) && !_.isNull(value);

/**
 * URl-хелпер для преобразования в маршрут для Yii2.
 *
 * @param route Маршрут для обработки.
 * @returns {string} Обработанный маршрут.
 */
export const urlHelper = (route) => location.pathname.replace(/[\w-]+\/[\w-]+\\*$/, route);
