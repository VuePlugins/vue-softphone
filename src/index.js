/**
 * Плагин для Vue.js для управления утилитой SoftPhone, начиная с версии 6.3.
 *
 */
const axios = require('axios');
const helpers = require("./shared");

const VueSoftphone = {
    /**
     * Иньекция плагина.
     *
     * @param Vue Vue.js версии 5.2.+
     * @param options Начальные параметры.
     */
    install: function (Vue, options) {
        Vue.mixin({
            mounted() {
                // console.info('SoftPhone Vue plugin is loading...');
                this.$softphone.init();
                // console.info('SoftPhone Vue plugin was loaded!');
            }
        });
        /**
         * Определяем плагин как $softphone.
         *
         * @type {{data: {actionRoutes: {transferCreate: {url: string}, toneDial: {url: string}, closeCall: {url: string}}}, transferCloseHelper: function(*, *=, *=), setStatus: function(*=, *=, *=), originalTransferFunction: function(), originalTransferWithCallBackFunction: function(), originalConsultationTransferFunction: function(), originalToneDialFunction: function(), getCallId: function(): *, canClose: function(*=), closeOnCallEnd: function(*=), blockClosing: function(): (blockClosing|(function())), submitTimeout: function(*=), workType: function(*=), defaultWorkType: function(): (defaultWorkType|(function())), factualCompletion: function(): (factualCompletion|(function())), formClosing: function(): (formClosing|(function())), limitValueArrived: function(): (limitValueArrived|(function())), submitForm: function(*=), wrapupTime: function(): (wrapupTime|(function())), idleHold: function(): (idleHold|(function())), init(): boolean, sendToCallBack(*=, *=): void, checkClientCallId(): boolean, initSoftPhone(): boolean, isInSoftPhone(*=): boolean, setClientCallId(*=): boolean}}
         */
        Vue.prototype.$softphone = {
            data: {
                ...{
                    client: {
                        clientCallId: window.ID_CALL
                    },
                    actionRoutes: {
                        transferCreate: {
                            url: 'call/transfer-create'
                        },
                        toneDial: {
                            url: 'call/tone-dial'
                        },
                        closeCall: {
                            url: 'call/close'
                        }
                    }
                },
                ...options
            },

            /**
             * Закрыть трансфер.
             *
             * @param transferResult Результат трансфера.
             * @param clientCallId Call id клиента.
             *
             * @param callback callback-функция.
             */
            transferCloseHelper: (transferResult, clientCallId = undefined, callback = undefined) =>
                axios.get(
                    'call/transfer-close',
                    {
                        params: {
                            transferResult,
                            ID_CALL: helpers.isDefined(clientCallId) ? clientCallId : window.clientCallId
                        }
                    }
                )
                    .then(response => this.sendToCallBack(response.data, callback))
                    .catch(error => console.warn('Ошибки при отправке запроса на "Закрытие трансфера"!', error)),

            /**
             * Установит статус клиента.
             *
             * @param status Статус.
             * @param clientCallId Call Id клиента.
             *
             * @param callback callback-функция.
             */
            setStatus: (status, clientCallId = undefined, callback = undefined) =>
                axios.get(
                    'call/set-status',
                    {
                        params: {
                            ID_STATUS: status,
                            ID_CALL: helpers.isDefined(clientCallId) ? clientCallId : window.clientCallId
                        }
                    }
                )
                    .then(response => this.sendToCallBack(response.data, callback))
                    .catch(error => console.warn('Ошибки при отправке запроса на "Установку статуса"!', error)),

            /**
             * Оригинальная функция трансфера.
             */
            originalTransferFunction: () => {
            },

            /**
             * Оригинальная функция трансфера с callback'ом.
             */
            originalTransferWithCallBackFunction: () => {
            },

            /**
             * Оригинальная функция транфсера-консультации.
             */
            originalConsultationTransferFunction: () => {
            },

            /**
             * Оригинальная функция тонового набора.
             */
            originalToneDialFunction: () => {
            },

            /**
             * Идентификатор вызова (то же что и атрибут eggId
             * объекта EggPrototype).
             * @returns {*}
             */
            getCallId: () => phoneControllerService.callId,

            /**
             * Указывает на то, разрешено ли закрывать эту
             * страницу
             * @param value
             * @returns {undefined|*}
             */
            canClose: (value = undefined) => {
                if (value === undefined) {
                    return phoneControllerService.canClose;
                } else {
                    phoneControllerService.canClose = value;
                    return phoneControllerService.canClose;
                }
            },
            /**
             * SoftPhone завершает вызов с идентификатором, заданным
             * параметром call_id.
             *
             * @returns {boolean}
             */
            hangup() {
                phoneControllerService.hangup();
                return true;
            },
            /**
             * Завершение вызова, находящегося в
             * поствызывной обработке.
             */
            closeCall() {
                phoneControllerService.close()
            },
            /**
             * Указывает на то, нужно ли удалять вызов по его
             * завершению.
             *
             * Примечание Если вызов уже завершён, то при
             * присвоении данному параметру значения true ,
             * закрытие страницы не произойдёт, так же если
             * параметр canClose имеет значение false , то
             * закрытие страницы также не произойдёт
             *
             * @param value
             * @returns {undefined|*}
             */
            closeOnCallEnd: (value = undefined) => {
                if (value === undefined) {
                    return phoneControllerService.closeOnCallEnd;
                } else {
                    phoneControllerService.closeOnCallEnd = value;
                    return phoneControllerService.closeOnCallEnd;
                }
            },

            /**
             * Указывает на то, разрешено ли блокировать закрытие
             * страницы (устанавливается через параметры
             * ActiveXParams).
             *
             * @returns {blockClosing|(function())}
             */
            blockClosing: () => phoneControllerService.blockClosing,

            /**
             * Время для поствызывной обработки в режиме работы
             * limitValueArrived.
             *
             * @param value
             * @returns {undefined|*}
             */
            submitTimeout: (value = undefined) => {
                if (value === undefined) {
                    return phoneControllerService.submitTimeout;
                } else {
                    phoneControllerService.submitTimeout = value;
                    return phoneControllerService.submitTimeout;
                }
            },

            /**
             * Режим работы.
             *
             * @param value
             * @returns {undefined|*}
             */
            workType: (value = undefined) => {
                if (value === undefined) {
                    return phoneControllerService.workType;
                } else {
                    phoneControllerService.workType = value;
                    return phoneControllerService.workType;
                }
            },

            /**
             * Режим работы по умолчанию.
             *
             * @returns {defaultWorkType|(function())}
             */
            defaultWorkType: () => phoneControllerService.defaultWorkType,

            /**
             * Идентификатор режима работы factualCompletion.
             *
             * @returns {factualCompletion|(function())}
             */
            factualCompletion: () => phoneControllerService.factualCompletion,

            /**
             * Идентификатор режима работы formClosing.
             *
             * @returns {formClosing|(function())}
             */
            formClosing: () => phoneControllerService.formClosing,

            /**
             * Идентификатор режима работы limitValueArrived.
             *
             * @returns {limitValueArrived|(function())}
             */
            limitValueArrived: () => phoneControllerService.limitValueArrived,

            /**
             * Указывает на то, нужно ли при завершении вызова
             * эмулировать нажатие на кнопку с id 'save', если она
             * присутствует на форме.
             *
             * @param value
             * @returns {undefined|*}
             */
            submitForm: (value = undefined) => {
                if (value === undefined) {
                    return phoneControllerService.submitForm;
                } else {
                    phoneControllerService.submitForm = value;
                    return phoneControllerService.submitForm;
                }
            },

            /**
             * Время поствызывной обработки.
             *
             * @returns {wrapupTime|(function())}
             */
            wrapupTime: () => phoneControllerService.wrapupTime,

            /**
             * Указывает на то, что удержание вызова не
             * производится.
             *
             * @returns {idleHold|(function())}
             */
            idleHold: () => phoneControllerService.idleHold,

            /**
             * Осуществление исходящего вызова на номер number.
             *
             * @param number
             */
            call(number) {
                phoneControllerService.call(number);
            },

            /**
             * Изменение состояния SoftPhone на newState с причиной reason.
             * Параметр newState может содержать следующие значения:
             *
             * 'normal'
             * 'away'
             * 'dnd'
             * 'custom1'
             * 'custom2'
             * 'custom3'
             *
             * @param newState
             * @param reason
             */
            changeState(newState, reason) {
                phoneControllerService.changeState(newState, reason)
            },

            /**
             * Выход из SoftPhone.
             */
            logout() {
                phoneControllerService.logout();
            },

            /**
             * Закрытие приложения SoftPhone.
             */
            exit() {
                phoneControllerService.exit()
            },

            /**
             * Переоткрытие звукового устройства.
             */
            reopenAudioDevice() {
                phoneControllerService.reopenAudioDevice()
            },

            /**
             * Отправка сообщения (alert) шине, где level может принимать
             * следующие значения:
             *
             * CRIT — 0;
             * ERROR — 1;
             * WARN — 2;
             * INFO — 3;
             * DEBUG — 4.
             *
             * @param level
             * @param message
             * @param service
             */
            sendAlert(level, message, service) {
                phoneControllerService.sendAlert(level, message, service)
            },

            /**
             * Отправка факса на номер number , где fileOrUrl — путь до
             * локально расположенного файла, или URL (осуществление
             * вызова на указанный номер и отправка факса).
             *
             * @param number
             * @param fileOrUr
             */
            sendFaxOnNumber(number, fileOrUr) {
                phoneControllerService.sendFaxOnNumber(number, fileOrUr);
            },

            /**
             * Отправка текстового сообщения в вызов (по протоколу SIP/H323).
             *
             * @param message
             */
            sendVoipMessage(message) {
                phoneControllerService.sendVoipMessage(message);
            },

            /**
             * Отправка записи на вкладку Отчёт в SoftPhone
             *
             * @param level
             * @param message
             * @param params
             */
            oftphoneLog(level, message, ...params) {
                phoneControllerService.oftphoneLog(level, message, ...params);
            },

            /**
             * Возвращает параметр вызова с именем key.
             *
             * @param key
             */
            getPageParam(key) {
                phoneControllerService.getPageParam(key);
            },

            /**
             * Задает параметру с именем key значением value.
             *
             * @param key
             * @param value
             */
            setPageParam(key, value) {
                phoneControllerService.setPageParam(key, value);
            },

            /**
             * Включение/выключение режима безопасности (отключение записи
             * разговора). Параметр enable может принимать следующие
             * значения:
             *
             * true — включение режима безопасности.
             * false — выключение режима безопасности (по умолчанию).
             *
             * Примечание Метод, без каких-либо последствий, может быть
             * вызван несколько раз подряд.
             *
             * @param enable
             */
            setSessionSecureMode(enable) {
                phoneControllerService.setSessionSecureMode(enable);
            },

            /**
             * Инициализация и применение основных настроек сервиса.
             */
            init() {
                // Проверяем/инициализируем экземпляр SoftPhone.
                const modulesChecking = this.isInSoftPhone();
                // если что-то пошло не так...
                if (!modulesChecking) {
                    alert('Проблемой с загрузкой модулей!');
                    return false;
                }

                /**
                 * Создание функций хелперов.
                 */
                if (helpers.isDefined(phoneControllerService.onStatusChange)) {
                    phoneControllerService.onStatusChange = (status) => this.setStatus(status);
                }

                if (helpers.isDefined(phoneControllerService.onStatusChanged)) {
                    phoneControllerService.onStatusChanged = (status) => this.setStatus(status);
                }

                phoneControllerService.onDropCall = () =>
                    axios.get(
                        this.data.actionRoutes.closeCall.url,
                        {
                            params: {
                                ID_CALL: this.data.client.clientCallId
                            }
                        }
                    )
                        .then(response => this.sendToCallBack(response.data, callback))
                        .catch(error => console.warn('Ошибки при отправке запроса на "Закрытие звонка"!', error));

                /**
                 * Функция трансфера.
                 *
                 * @param phone Номер телефона.
                 */
                phoneControllerService.transfer = (phone) => {

                    phoneControllerService.currentTransferType = 'Обычное перенаправление';
                    phoneControllerService.currentTransferPhone = phone;

                    this.originalTransferFunction(phone);

                };
                /**
                 * Трансфер с callback.
                 *
                 * @param phone Номер телефона.
                 */
                phoneControllerService.transferWithCallBack = (phone) => {

                    phoneControllerService.currentTransferType = 'Перенаправление с возвратом';
                    phoneControllerService.currentTransferPhone = phone;

                    this.originalTransferWithCallBackFunction(phone);

                };

                /**
                 * Трансфер с консультацией.
                 *
                 * @param phone Номер телефона.
                 */
                phoneControllerService.consultationTransfer = (phone) => {

                    phoneControllerService.currentTransferType = 'Перенаправление с консультацией';
                    phoneControllerService.currentTransferPhone = phone;

                    this.originalConsultationTransferFunction(phone);

                };

                this.originalTransferFunction = phoneControllerService.transfer;
                this.originalTransferWithCallBackFunction = phoneControllerService.transferWithCallBack;
                this.originalConsultationTransferFunction = phoneControllerService.consultationTransfer;
                this.originalToneDialFunction = phoneControllerService.toneDial;

                /**
                 * Обработка трансфера.
                 */
                phoneControllerService.onTransferCall = () => {
                    axios.get(
                        this.data.actionRoutes.transferCreate.url,
                        {
                            params: {
                                transferType: phoneControllerService.currentTransferType,
                                transferPhone: phoneControllerService.currentTransferPhone,
                                ID_CALL: this.data.client.clientCallId
                            }
                        }
                    )
                        .then(response => {

                            phoneControllerService.currentTransferType = null;
                            phoneControllerService.currentTransferPhone = null;

                            this.sendToCallBack(response.data, callback)
                        })
                        .catch(error => console.warn('Ошибки при отправке запроса на "Закрытие звонка"!', error));
                };

                // Успешный трансфер.
                phoneControllerService.onTransferSucceed = () => this.transferCloseHelper('Удачно');
                // С ошибкой.
                phoneControllerService.onTransferFailed = () => this.transferCloseHelper('Не удачно');
                // Был возвращён.
                phoneControllerService.onTransferCallReturned = () => this.transferCloseHelper('Возвращен');

                /**
                 * Тоновый набор.
                 *
                 * @param phone Номер телефона.
                 */
                phoneControllerService.toneDial = (phone) => {
                    axios.get(
                        this.data.actionRoutes.toneDial.url,
                        {
                            PHONE: phone,
                            ID_CALL: this.data.client.clientCallId
                        }
                    );
                    this.originalToneDialFunction(phone);
                }
            },

            /**
             * Передать параметр в callback.
             *
             * @param data Данные для прокидки.
             * @param callback callback-функция.
             */
            sendToCallBack(data, callback) {
                if (helpers.isDefined(callback)) {
                    callback(data);
                }
            },

            /**
             * Проверка на наличие установленного callId.
             *
             * @returns {boolean}
             */
            checkClientCallId() {
                return helpers.isDefined(this.data.client.clientCallId);
            },

            /**
             * Инициализация SoftPhone.
             *
             * @returns {boolean} Результат инициализации.
             */
            initSoftPhone() {
                try {
                    window.phoneControllerService = {
                        onDropCall() {
                        },
                        close() {
                            this.onDropCall();
                            window.close();
                        },
                        transfer() {
                        },
                        transferWithCallBack() {
                        },
                        consultationTransfer() {
                        },
                        toneDial() {
                        },
                        setMusicOnHold() {
                        }
                    };
                }
                catch (exception) {
                    console.warn('Не удалось создать экземпляр SoftPhone!', exception);
                    return false;
                }

                console.warn(`Утилита SofPhone не была обнаружена! Создаём свой экземпляр, мало ли...`);
                return true;
            },

            /**
             * Проверка на наличие основного объекта SoftPhone: phoneControllerService.
             *
             * @param createIfNotExists Если объект не найден, создать его или нет.
             * @returns {boolean} Результат.
             */
            isInSoftPhone(createIfNotExists = true) {
                let softPhoneExists = typeof phoneControllerService !== 'undefined';
                if (!softPhoneExists && createIfNotExists) {
                    softPhoneExists = this.initSoftPhone();
                }
                return softPhoneExists;
            },

            /**
             * Установка CallId клиента.
             *
             * @param newClientCallId CallId клиента.
             * @returns {boolean} Результат.
             */
            setClientCallId(newClientCallId) {

                if (!helpers.isDefined(newClientCallId))
                    return false;

                this.data.client.clientCallId = newClientCallId;

                return true;
            }
        }
    }
};

export default VueSoftphone;