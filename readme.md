# Установка
Для установки плагина, необходимо выполнить команду:
````bash
	npm install https://gitlab.com/VuePlugins/vue-softphone
````
# Использование
Подключение в коде проекта
````javascript
	// Импорт плагина
	import VueSoftphone from 'vue-softphone';  
    // Подключение плагина к Vue.
    Vue.use(VueSoftphone);
````

# API

## Helper Functions

> Функции-хелперы для `backend` на `Yii2` содержащие в себе обращения к таким маршрутам как:
>   - `call/transfer-close`
>   - `call/set-status`
>   
>   P.S. Для использования,  нужны соответствующие маршруты в роутере на `backend`.

````javascript
	/**
     * Закрыть трансфер.
     *
     * @param transferResult Результат трансфера.
     * @param clientCallId Call id клиента.
     *
     * @param callback callback-функция.
     */
	function transferCloseHelper(transferResult, clientCallId = undefined, callback = undefined)
````

````javascript
	/**
     * Установит статус клиента.
     *
     * @param status Статус.
     * @param clientCallId Call Id клиента.
     *
     * @param callback callback-функция.
     */
    function setStatus(status, clientCallId = undefined, callback = undefined)
````

## Phone Controller Service

> Список ниже, составлен из наиболее используемых функций.
> 
> P.S. Плагин поддерживает большинство основных команд объекта `phoneControllerService`.

````javascript
	/**
     * Указывает на то, разрешено ли закрывать эту
     * страницу.
     * 
     * @param value
     * @returns {undefined|*}
     */
    function canClose(value = undefined)
````

````javascript
	 /**
      * SoftPhone завершает вызов с идентификатором, заданным
      * параметром call_id.
      *
      * @returns {boolean}
      */
     function hangup()
````

````javascript
	 /**
      * Завершение вызова, находящегося в
      * поствызывной обработке.
      */
     function closeCall()
````

````javascript
	 /**
      * Осуществление исходящего вызова на номер number.
      *
      * @param number Номер телефона, на который осуществить звонок.
      */
     function call(number)
````

````javascript
	 /**
      * Изменение состояния SoftPhone на newState с причиной reason.
      * Параметр newState может содержать следующие значения:
      *
      * 'normal'
      * 'away'
      * 'dnd'
      * 'custom1'
      * 'custom2'
      * 'custom3'
      *
      * @param newState Новое состояние.
      * @param reason Причина перехода.
      */
     function changeState(newState, reason)
````

````javascript
	 /**
      * Переоткрытие звукового устройства.
      */
     function reopenAudioDevice()
````

````javascript
	 /**
      * Выход из SoftPhone.
      */
     function logout()
````

````javascript
	 /**
      * Закрытие приложения SoftPhone.
      */
     function exit()
````